window.onload = init;

function init() {
    document.getElementById('connexionEmail').addEventListener('input', MemeEmail);
    document.getElementById('connexionMdp').addEventListener('input', MemeMdP);
    document.getElementById('valider').addEventListener('click', resterPage)
}

var emailPareil = false;
var MdPPareil = false;

function MemeEmail(){
    let saisie = document.getElementById('connexionEmail').value;
    let email = localStorage.getItem('Email');
    console.log(email);
    if ( saisie !== email){
        emailPareil = false;
        verifBouton()
    } else {
         emailPareil = true;
         verifBouton()
    }
}

function MemeMdP(){
    let saisie = document.getElementById('connexionMdp').value;
    let mdp = localStorage.getItem('mdp');
    console.log(mdp);
    if ( saisie !== mdp){
        MdPPareil = false;
        verifBouton()     
    } else {
        MdPPareil = true;
        verifBouton()
    }
}

function verifBouton(){
    if (emailPareil === true && MdPPareil === true){
        document.getElementById('valider').disabled = false;
    } else {
        document.getElementById('valider').disabled = true;
    }
}

function resterPage(event){
        alert("L'email " + document.getElementById('connexionEmail').value + " est connecté" )
        document.location.href="connexion.html";
        event.preventDefault();
    }
