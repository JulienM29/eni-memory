const tabImg = [
	"./image/dinosaures/1.jpg",
	"./image/dinosaures/2.jpg",
	"./image/dinosaures/3.jpg",
	"./image/dinosaures/4.jpg",
	"./image/dinosaures/5.jpg",
	"./image/dinosaures/6.jpg",
	"./image/dinosaures/7.jpg",
	"./image/dinosaures/8.jpg",
	"./image/dinosaures/1.jpg",
	"./image/dinosaures/2.jpg",
	"./image/dinosaures/3.jpg",
	"./image/dinosaures/4.jpg",
	"./image/dinosaures/5.jpg",
	"./image/dinosaures/6.jpg",
	"./image/dinosaures/7.jpg",
	"./image/dinosaures/8.jpg",
]
let carteA;
let carteB;
let locked = false
let compteur =0;
let compteurVictoire =0;
var nombreCoups = 0;

window.onload = init;

function init() {
	aleatoireImg(tabImg);
	let cartes = document.querySelectorAll('#tableau .case');
	for (let i = 0; i < cartes.length; i++) {
		cartes[i].addEventListener('click', retournerCarte);
	}
	carteA = null; // variable A
	carteB = null; // variable B
	
}

function aleatoireImg(tabImg) {
	var i, j, tmp;
	for (i = tabImg.length - 1; i > 0; i--) {
		j = Math.floor(Math.random() * (i + 1));
		tmp = tabImg[i];
		tabImg[i] = tabImg[j];
		tabImg[j] = tmp;
	}
	console.log(tabImg);
	return tabImg;
}


function retournerCarte(event) {
	if (!locked) { // boolean permettant de bloquer une partie de la fonction , après la fct comparé on le remet en faux pour remettre la fonction active
		
		let carte = event.target;
		if (carte.classList.contains('case')) {
			carte.classList.remove('src');
			carte.setAttribute('src', tabImg[parseInt(carte.id)]);
			carte.animate([
				{transform: "rotateY(180deg)"},
				{transform: "rotateY(0deg)"}
			],
			{
				duration: 400,
			})
			carte.removeEventListener ('click', retournerCarte) // empêche un clic sur la même carte
			
			if (carteA === null || carteA === undefined) { // null ou undefined car il l'a marqué comme ça sur la console
				carteA = carte; // mettre la valeur dans la variable 1
				console.log("carte a" + tabImg[parseInt(carteA.id)]);
			} else if (carteA !== null || carteA !== undefined) {
				carteB = carte // mettre la valeur dans la variable 2
				console.log("carte b" + tabImg[parseInt(carteB.id)]);
				compteur++;
				document.getElementById('afficherCoups').textContent ="Nombre de coups : " + compteur;
				locked = true;
				setTimeout(() => {
					compare();
					locked = false; //faire une fonction fléchée qui prends les 2 car sinon il va locked en même temps et on pourra quand même cliquer sur d'autres cartes
				}, 750)
			}
		} else {
			carte.classList.add('case');
			carte.setAttribute('src', './image/dinosaures/fond.jpg');
		}
	console.log(compteur);
}
}
 
function compare() {
	
	var a = tabImg[parseInt(carteA.id)]
	var b = tabImg[parseInt(carteB.id)]
	if (a === b) {
		carteA.classList.add('trouver');
		carteB.classList.add('trouver');
		carteA = null;
		carteB = null;
		compteurVictoire++;
		victoire();
	} else {
		carteA.classList.add('case');
		carteA.setAttribute('src', './image/dinosaures/fond.jpg');
		carteA.addEventListener('click', retournerCarte);
		carteA.animate([
			{transform: "rotateY(180deg)"},
			{transform: "rotateY(0deg)"}
		],
		{
			duration: 400,
		})
		carteB.classList.add('case');
		carteB.setAttribute('src', './image/dinosaures/fond.jpg');
		carteB.addEventListener('click', retournerCarte);
		carteB.animate([
			{transform: "rotateY(180deg)"},
			{transform: "rotateY(0deg)"}
		],
		{
			duration: 400,
		})
		carteA = null;
		carteB = null;
	}
}

function victoire (){
	var compteurFinal = compteur;
	if (compteurVictoire === 8){
		alert("bien joué vous avez gagné en : " + compteurFinal + "coups ! \n Appuyez sur la barre espace pour relancer ")
		// on passe a la relance
		document.addEventListener('keydown', newGame )
		function newGame (event){
			if (event.code === 'Space') {
			 let cartes = document.querySelectorAll('#tableau .trouver');
			cartes.forEach(x => {
				x.animate([
					{transform: "rotateY(180deg)"},
					{transform: "rotateY(0deg)"}
				],{
					duration: 400,
				})
			 x.classList.remove('trouver')
			 x.classList.add('case')
			 x.setAttribute('src', "./image/dinosaures/fond.jpg")
			
			init();
			  }
			 )
			compteur =0;
			document.getElementById('afficherCoups').textContent ="Nombre de coups : " + compteur;
			compteurFinal =0;
			compteurVictoire =0;}
			}
	}

}
