window.onload = init;

function init() {
    afficherUtilisateur();
    afficherEmail();
    document.getElementById('select').addEventListener('change', afficherDino)
    document.getElementById('taille').addEventListener('change', afficherTaille)

}

function afficherUtilisateur() {
    let BaliseNom = document.getElementById('nomUtilisateur');
    let utilisateurNom = window.localStorage.getItem('nom');
    BaliseNom.innerText = utilisateurNom;
}

function afficherEmail() {
    let BaliseEmail = document.getElementById('emailProfil');
    let utilisateurEmail = window.localStorage.getItem('Email');
    BaliseEmail.innerText = utilisateurEmail;
}

function afficherDino() {

    var index = document.getElementById("select").selectedIndex;
    console.log(index);
     if (index === 0) {
        document.getElementById('dinoProfil').src = "./image/dinosaures/memory_detail_dinosaures.png"
        document.getElementById('dinoProfil').style.visibility = "visible";
    } else if (index === 1) {
        document.getElementById('dinoProfil').src = "./image/animaux/memory_detail_animaux.png"
        document.getElementById('dinoProfil').style.visibility = "visible";
    }

}

function afficherTaille() {

    var indexTaille = document.getElementById("taille").value;
    console.log(indexTaille);

    document.getElementById('tailleProfil').src = "./image/dinosaures/memory" + indexTaille + ".png"
    document.getElementById('tailleProfil').style.visibility = "visible";

    if(indexTaille === "none"){
        document.getElementById('tailleProfil').style.visibility = "hidden";
    }


}


