window.onload = init;

function init() {
    verifFormIdOK();
    document.getElementById('identifiant').addEventListener('input', verifIdentifiant);
    document.getElementById('email').addEventListener('input', verifierEmail);
    document.getElementById('mdp').addEventListener('input', verifierMdP);
    document.getElementById('mdpVerif').addEventListener('input', verifMemeMdP);
    document.getElementById('valider').addEventListener('click', Enregistrer);
    

}


var idIsOK = false;
var mailIsOK = false;
var mdpIsOK = false;
var mdpdeuxIsOK = false;

function verifIdentifiant(event) {

    let saisie = event.currentTarget.value;
    let tailleOK = verifierTaille(saisie);
    if (tailleOK) {
        let consigneTaille = document.getElementById('identifiant');
        passerVertLog(consigneTaille);
        idIsOK = true;
        verifFormIdOK();
    } else {
        let consigneTaille = document.getElementById('identifiant');
        passerRougeLog(consigneTaille);
        idIsOK = false;
        verifFormIdOK();
    }
}

function verifierEmail(event) {

    var regex = new RegExp(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/);
    let saisie = event.currentTarget.value;
    if (regex.test(saisie)) {
        let consigneEmail = document.getElementById('email');
        passerVertMail(consigneEmail);
        mailIsOK = true;
        verifFormIdOK();
    } else {
        let consigneEmail = document.getElementById('email');
        passerRougeMail(consigneEmail);
        mailIsOK = false;
        verifFormIdOK();
    }

}

function verifierMdP(event) {
    let saisie = event.currentTarget.value;
    let tailleOK = verifierTailleMdp(saisie)
    let symboleOK = verifierSpeciaux(saisie)
    let chiffresOK = verifierChiffre(saisie)

    if (tailleOK && symboleOK && chiffresOK) {
        let consigneMdp = document.getElementById('mdp');
        passerVertMdp(consigneMdp);
        mdpIsOK = true;
        verifFormIdOK();
    } else {
        let consigneMdp = document.getElementById('mdp');
        passerRougeMdp(consigneMdp);
        mdpIsOK = false;
        verifFormIdOK();
    }
}

function verifMemeMdP() {
    if (document.getElementById('mdp').value === document.getElementById('mdpVerif').value) {
        let consigneMdp = document.getElementById('mdpVerif');
        passerVertVerifMdp(consigneMdp);
        mdpdeuxIsOK = true;
        verifFormIdOK();
    } else {
        let consigneMdp = document.getElementById('mdpVerif');
        passerRougeVerifMdp(consigneMdp);
        mdpdeuxIsOK = false;
        verifFormIdOK();
    }
}

function verifFormIdOK() {
    var formIsOK = idIsOK && mdpIsOK && mailIsOK && mdpdeuxIsOK;
    if (formIsOK) {
        document.getElementById('valider').disabled = false;
    } else {
        document.getElementById('valider').disabled = true;
    }
}

function Enregistrer(event){
    let saisieNom = document.getElementById('identifiant').value
    let saisieEmail = document.getElementById('email').value
    let saisieMdp = document.getElementById('mdp').value
    localStorage.setItem('nom', saisieNom);
    localStorage.setItem('Email', saisieEmail);
    localStorage.setItem('mdp', saisieMdp);
    alert("L'email : "+saisieEmail +" est enregistré !")
    window.location.href="./connexion.html";
    event.preventDefault()
}



function verifierChiffre(saisie) {
    return /[0-9]/g.test(saisie)
}

function verifierTaille(saisie) {
    return saisie.length >= 3;
}
function verifierTailleMdp(saisie) {
    return saisie.length >= 6;
}
function verifierSpeciaux(saisie) {
    return /\@+|\%+|\&+|\!+|\\+|\~+/.test(saisie);
}



function passerVertLog() {
    document.getElementById('imgLog').src = './image/check.svg';
    document.getElementById('imgLog').style.visibility = "visible";
}
function passerRougeLog() {
    document.getElementById('imgLog').src = './image/error.svg';
    document.getElementById('imgLog').style.visibility = "visible";

}

function passerVertMail() {
    document.getElementById('imgMail').src = './image/check.svg';
    document.getElementById('imgMail').style.visibility = "visible";

}
function passerRougeMail() {
    document.getElementById('imgMail').src = './image/error.svg';
    document.getElementById('imgMail').style.visibility = "visible";

}

function passerVertMdp() {
    document.getElementById('imgMdp').src = './image/check.svg';
    document.getElementById('imgMdp').style.visibility = "visible";

}
function passerRougeMdp() {
    document.getElementById('imgMdp').src = './image/error.svg';
    document.getElementById('imgMdp').style.visibility = "visible";

}

function passerVertVerifMdp() {
    document.getElementById('imgVerifMdp').src = './image/check.svg';
    document.getElementById('imgVerifMdp').style.visibility = "visible";

}
function passerRougeVerifMdp() {
    document.getElementById('imgVerifMdp').src = './image/error.svg';
    document.getElementById('imgVerifMdp').style.visibility = "visible";

}



